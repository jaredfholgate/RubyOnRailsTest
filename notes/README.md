To configure PostGreSQL, you need to update the pg_hb.conf file as per the example in this folder. You can then run;

rake db:create
rake db:migrate

And you should get a success.

For this install, I also had to create a sys admin user called 'jared' via the pgAdmin tool...